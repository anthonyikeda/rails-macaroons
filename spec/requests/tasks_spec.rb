require "rails_helper"

RSpec.describe "Tasks API", type: :request do
  let!(:tasks) { create_list(:task, 10) }
  let(:task_id) { tasks.first.id }

  describe "GET /task" do
    before { get "/task" }

    it "returns tasks" do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it "returns status code 200" do
      expect(response).to have_http_status(200)
    end
  end

  describe "GET /task/:id" do
    before { get "/task/#{task_id}" }

    context "when the record exists" do
      it "returns the task" do
        expect(json).not_to be_empty
        expect(json["id"]).to eq(task_id)
      end

      it "returns status code 200" do
        expect(response).to have_http_status(200)
      end
    end

    context "when the record does not exist" do
      let(:task_id) { 100 }
      it "returns a status of 404" do
        expect(response).to have_http_status(404)
      end

      it "returns a not found message" do
        expect(response.body).to match(/Couldn't find Task/)
      end
    end
  end

  describe "POST /task" do
    let(:valid_attributes) { {name: "Do the laundry", description: "Make sure the clothes are clean"} }

    context "when the request is valid" do
      before { post "/task", params: valid_attributes }

      it "creates a task" do
        expect(json["name"]).to eq("Do the laundry")
      end

      it "returns status code 201" do
        expect(response).to have_http_status(201)
      end
    end
  end
end
