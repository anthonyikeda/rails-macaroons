Rails.application.configure do
  puts ".........Starting Lograge config"
  config.lograge.enabled = true
  # config.lograge.formatter = Lograge::Formatters::Logstash.new

  puts ".........Setting Lograge formatter to GrayLog2"
  config.lograge.formatter = Lograge::Formatters::Logstash.new
  # config.lograge.logger = ActiveSupport::Logger.new(STDOUT)
  config.lograge.logger = LogStashLogger.new(type: :udp, host: ENV["LOGSTASH_HOST"], port: 5228)
end
