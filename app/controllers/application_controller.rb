require "macaroons"

class ApplicationController < ActionController::API
  include Response
  include ExceptionHandler

  attr_reader :current_user

  private

  def authenticate_user!
    _, token = request.headers["Authorization"].split(" ")
    logger.debug "Token: #{token}"
    @m = Macaroon.from_binary(token)
    begin
      v = Macaroon::Verifier.new()
      v.verify(macaroon: @m, key: "animals")
      @current_user = {id: 5}
    rescue => exception
      logger.error "Error verifying macaroon"
      render json: {error: "Invalid Authorization token"}, status: :unauthorized
    end
  end
end
